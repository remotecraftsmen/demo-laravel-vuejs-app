import { mount, createLocalVue } from '@vue/test-utils';
import expect from 'expect';
import ConfirmationModal from '../../resources/js/components/ConfirmationModal';
import Vue from 'vue';
import Vuetify from 'vuetify';

Vue.use(Vuetify);

const localVue = createLocalVue();

describe('ConfirmationModal', () => {
    let vuetify;
    let wrapper;

    beforeEach(() => {
        vuetify = new Vuetify();
        global.requestAnimationFrame = cb => cb();

        wrapper = mount(ConfirmationModal, {
            localVue,
            vuetify,
            propsData: {
                dialog: true,
                message: 'Are you sure you want to delete this estimate?'
            }
        });
    });

    it('renders a vue instance', () => {
        expect(wrapper.isVueInstance()).toBe(true);
    });

    it('display text with question', () => {
        expect(wrapper.html()).toContain(
            'Are you sure you want to delete this estimate?'
        );
    });

    it('have two buttons ', () => {
        expect(wrapper.findAll('.v-btn').length).toBe(2);
    });

    it('emits "confirm" event after clicking acceptance button', () => {
        const buttons = wrapper.findAll('.v-btn');
        buttons.at(1).trigger('click');

        expect(wrapper.emitted('confirmed')).toBeTruthy();
    });

    it('emits "close" event after clicking disagree button', () => {
        const buttons = wrapper.findAll('.v-btn');
        buttons.at(0).trigger('click');

        expect(wrapper.emitted('close')).toBeTruthy();
    });
});
