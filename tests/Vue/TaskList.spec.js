import { mount, createLocalVue } from '@vue/test-utils';
import expect from 'expect';
import moxios from 'moxios';
import TaskList from '../../resources/js/views/TasksList';
import VueFlashMessage from 'vue-flash-message';
import Vue from 'vue';
import Vuetify from 'vuetify';

Vue.use(Vuetify);

const localVue = createLocalVue();
localVue.use(VueFlashMessage);

describe('TodoList', () => {
    let wrapper;
    let vuetify;

    before(() => {
        moxios.stubOnce(
            'GET',
            '/api/tasks?page=1&perPage=15&q=&sortBy=id&descending=true',
            {
                status: 200,
                response: {
                    tasks: {
                        data: [
                            {
                                id: 0,
                                name: 'task1',
                                completed: 1
                            },
                            {
                                id: 1,
                                name: 'task2',
                                completed: 0
                            },
                            {
                                id: 2,
                                name: 'task3',
                                completed: 0
                            }
                        ],
                        last_page: 1,
                        current_page: 1
                    }
                }
            }
        );
    });

    beforeEach(done => {
        moxios.install();

        vuetify = new Vuetify();
        global.requestAnimationFrame = cb => cb();

        wrapper = mount(TaskList, {
            localVue,
            vuetify
        });

        moxios.wait(() => {
            wrapper.setData({
                loader: false
            });
            done();
        });
    });

    afterEach(() => {
        moxios.uninstall();
    });

    it('renders a vue instance', () => {
        expect(wrapper.isVueInstance()).toBe(true);
    });

    it('presents tasks in table', () => {
        wrapper.setData({
            tasks: [
                {
                    id: 1,
                    name: 'task1',
                    completed: 0
                },
                {
                    id: 2,
                    name: 'task2',
                    completed: 0
                },
                {
                    id: 3,
                    name: 'task3',
                    completed: 0
                }
            ]
        });

        expect(wrapper.contains('table#tasksTable')).toBe(true);

        wrapper.vm.tasks.forEach(task => {
            expect(wrapper.html()).toContain(task.name);
        });
    });

    it('presents loader when data is not loaded', () => {
        wrapper.setData({
            loader: true
        });

        expect(wrapper.contains('.loader-container')).toBe(true);
    });

    it('present check circle icon when status of item is completed', () => {
        wrapper.setData({
            tasks: [
                {
                    id: 0,
                    name: 'task1',
                    completed: 1
                }
            ]
        });

        expect(wrapper.find('td i.fa').classes()).toContain('fa-check-circle');
    });

    it('present empty circle icon when status of item is completed', () => {
        wrapper.setData({
            tasks: [
                {
                    id: 0,
                    name: 'task1',
                    completed: 0
                }
            ]
        });

        expect(wrapper.find('td i.fa').classes()).toContain('fa-circle');
    });

    it('present "Add New" button', () => {
        expect(wrapper.find('.v-btn').text()).toBe('Add new tasks');
    });

    it('can add new task after clicking "Add New" button and task has default parameters', done => {
        moxios.stubOnce('POST', '/api/tasks', {
            status: 200,
            response: {
                task: [
                    {
                        id: 1,
                        name: 'addedTask',
                        completed: 0
                    }
                ]
            }
        });

        moxios.stubOnce(
            'GET',
            '/api/tasks?page=1&perPage=15&q=&sortBy=id&descending=true',
            {
                status: 200,
                response: {
                    tasks: {
                        data: [
                            {
                                id: 1,
                                name: 'task1',
                                completed: 0
                            },
                            {
                                id: 2,
                                name: 'Please enter task name',
                                completed: 0
                            }
                        ],
                        current_page: 1,
                        last_page: 1
                    }
                }
            }
        );

        wrapper.setData({
            tasks: [
                {
                    id: 1,
                    name: 'task1',
                    completed: 0
                }
            ]
        });

        expect(wrapper.vm.tasks.length).toEqual(1);

        const addingButton = wrapper.find('.v-btn');
        addingButton.trigger('click');

        moxios.wait(() => {
            expect(wrapper.vm.tasks.length).toEqual(2);
            expect(wrapper.html()).toContain('Please enter task name');
            done();
        });
    });

    it('input shows after clicking on task name', () => {
        wrapper.setData({
            tasks: [
                {
                    id: 0,
                    name: 'task1',
                    completed: 0
                }
            ]
        });

        expect(wrapper.find('input[name="name"]').isVisible()).toBe(false);

        wrapper.find('table>tbody>tr>td>span').trigger('click');

        expect(wrapper.find('input[name="name"]').isVisible()).toBe(true);
    });

    it('can update task after clicking its name', done => {
        moxios.stubOnce('PUT', '/api/tasks/1', {
            status: 200,
            response: 1
        });

        wrapper.setData({
            tasks: [
                {
                    id: 1,
                    name: 'task1',
                    completed: 0,
                    path: '/tasks/1'
                }
            ]
        });

        expect(wrapper.html()).toContain('task1');

        const input = wrapper.find('input[name="name"]');
        input.setValue('updated todo');
        input.trigger('blur');

        moxios.wait(() => {
            expect(wrapper.vm.tasks[0].name).toEqual('updated todo');
            done();
        });
    });

    it('can update status of task after clicking its icon', done => {
        moxios.stubOnce('PATCH', '/api/tasks/1/toggle-status', {
            status: 200,
            response: {
                status: true,
                completed: 1
            }
        });

        wrapper.setData({
            tasks: [
                {
                    id: 1,
                    name: 'task1',
                    completed: 0,
                    path: '/tasks/1'
                }
            ]
        });

        expect(wrapper.html()).toContain('fa-circle');

        const statusIcon = wrapper.find('tbody tr td:nth-child(3) i');
        statusIcon.trigger('click');

        moxios.wait(() => {
            expect(wrapper.html()).toContain('fa-check-circle');
            done();
        });
    });
});
