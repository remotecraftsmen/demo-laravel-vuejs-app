<?php

namespace Tests\Feature;

use App\Article;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_see_articles_list()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->get('/articles');

        $response
            ->assertSuccessful()
            ->assertViewIs('articles.list');
    }

    public function test_attempt_to_see_articles_list_when_unlogged()
    {
        $response = $this->get('/articles');

        $response
            ->assertStatus(302)
            ->assertRedirect('/login');
    }

    public function test_attempt_to_get_articles_as_unlogged()
    {
        $response = $this->get('/api/articles');

        $response
            ->assertStatus(401)
            ->assertExactJson(['message' => 'Unauthorized', 'status' => 'error']);
    }

    public function test_get_all_articles()
    {
        [$john, $jane] = factory(User::class, 2)->create();

        factory(Article::class, 3)->create([
            'user_id' => $john->id,
        ]);

        factory(Article::class, 5)->create([
            'user_id' => $jane->id,
        ]);

        $response = $this->actingAs($john)->get('/api/articles?page=1&onlyMy=false');

        $response
            ->assertSuccessful()
            ->assertJsonFragment(['current_page' => 1])
            ->assertJsonFragment(['per_page' => 9])
            ->assertJsonFragment(['total' => Article::count()]);
    }

    public function test_get_only_my_articles()
    {
        [$john, $jane] = factory(User::class, 2)->create();

        factory(Article::class, 3)->create([
            'user_id' => $john->id,
        ]);

        factory(Article::class, 5)->create([
            'user_id' => $jane->id,
        ]);

        $response = $this->actingAs($john)->get('/api/articles?page=1&onlyMy=true');

        $response
            ->assertSuccessful()
            ->assertJsonFragment(['current_page' => 1])
            ->assertJsonFragment(['per_page' => 9])
            ->assertJsonFragment(['total' => 3]);
    }

    public function test_attempt_to_render_one_article_view_as_unlogged()
    {
        [$john, $jane] = factory(User::class, 2)->create();

        $article = factory(Article::class)->create([
            'user_id' => $john->id,
        ]);

        $response = $this->get('articles/' . $article->id);

        $response
            ->assertStatus(302)
            ->assertRedirect('/login');
    }

    public function test_render_one_article_view()
    {
        [$john, $jane] = factory(User::class, 2)->create();

        $article = factory(Article::class)->create([
            'user_id' => $john->id,
        ]);

        $response = $this->actingAs($jane)->get('articles/' . $article->id);

        $response
            ->assertSuccessful()
            ->assertViewIs('articles.show');
    }

    public function test_attempt_to_get_article_as_unlogged()
    {
        $john = factory(User::class)->create();

        $article = factory(Article::class)->create([
            'user_id' => $john->id,
        ]);

        $response = $this->get('/api/articles/' . $article->id);

        $response
            ->assertStatus(401)
            ->assertExactJson(['message' => 'Unauthorized', 'status' => 'error']);
    }

    public function test_get_one_article()
    {
        [$john, $jane] = factory(User::class, 2)->create();
        $title = 'title';
        $content = 'content';

        $article = factory(Article::class)->create([
            'user_id' => $john->id,
            'title' => $title,
            'content' => $content,
        ]);

        $response = $this->actingAs($jane)->get('/api/articles/' . $article->id);

        $response
            ->assertSuccessful()
            ->assertJsonFragment(['id' => $article->id])
            ->assertJsonFragment(['title' => $title])
            ->assertJsonFragment(['content' => $content])
            ->assertJsonFragment(['email' => $john->email]);
    }

    public function test_attempt_to_get_not_existing_article()
    {
        [$john, $jane] = factory(User::class, 2)->create();

        $article = factory(Article::class)->create([
            'user_id' => $john->id,
        ]);

        $response = $this->actingAs($jane)->get('/api/articles/0');

        $response
            ->assertStatus(404)
            ->assertSee('Can not find article entity.');
    }

    public function test_attempt_to_add_article_as_unlogged()
    {
        $response = $this->post('/api/articles', []);

        $response
            ->assertStatus(401)
            ->assertExactJson(['message' => 'Unauthorized', 'status' => 'error']);
    }

    public function test_add_article_without_image()
    {
        $articleCountBeforeAdd = Article::count();
        $john = factory(User::class)->create();
        $articleData = [
            'title' => 'title',
            'content' => 'content',
        ];

        $response = $this->actingAs($john)->post('/api/articles', $articleData);

        $response
            ->assertSuccessful()
            ->assertJsonFragment(['title' => $articleData['title']])
            ->assertJsonFragment(['content' => $articleData['content']])
            ->assertJsonFragment(['image' => null]);
        $this->assertSame($articleCountBeforeAdd, Article::count() - 1);
    }

    public function test_add_article_with_image()
    {
        $articleCountBeforeAdd = Article::count();
        $john = factory(User::class)->create();
        $articleData = [
            'title' => 'title',
            'content' => 'content',
            'image' => UploadedFile::fake()->image('avatar.jpg'),
        ];

        $response = $this->actingAs($john)->post('/api/articles', $articleData);

        $response
            ->assertSuccessful()
            ->assertJsonFragment(['title' => $articleData['title']])
            ->assertJsonFragment(['content' => $articleData['content']]);
        $this->assertSame($articleCountBeforeAdd, Article::count() - 1);
        Storage::disk('public')->assertExists($response->original->image);
    }

    public function test_attempt_to_add_article_without_title_and_content()
    {
        $articleCountBeforeAdd = Article::count();
        $john = factory(User::class)->create();
        $articleData = [];

        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])
            ->actingAs($john)
            ->json('POST', '/api/articles', $articleData);

        $response
            ->assertStatus(422)
            ->assertJsonFragment(['message' => 'The given data was invalid.'])
            ->assertJsonFragment(['title' => ['Title is required!']])
            ->assertJsonFragment(['content' => ['Content is required!']]);
    }

    public function test_attempt_to_add_article_with_image_as_string()
    {
        $articleCountBeforeAdd = Article::count();
        $john = factory(User::class)->create();
        $articleData = [
            'title' => 'title',
            'content' => 'content',
            'image' => 'image',
        ];

        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])
            ->actingAs($john)
            ->json('POST', '/api/articles', $articleData);

        $response
            ->assertStatus(422)
            ->assertJsonFragment(['message' => 'The given data was invalid.'])
            ->assertJsonFragment(['image' => ['Image must be an image!']]);
    }

    public function test_attempt_to_get_related_articles_as_unlogged()
    {
        $john = factory(User::class)->create();

        $article = factory(Article::class)->create([
            'user_id' => $john->id,
        ]);

        $response = $this->get('/api/articles/' . $article->id);

        $response
            ->assertStatus(401)
            ->assertExactJson(['message' => 'Unauthorized', 'status' => 'error']);
    }

    public function test_get_realted_articles()
    {
        [$john, $jane] = factory(User::class, 2)->create();

        $article = factory(Article::class)->create([
            'user_id' => $john->id,
        ]);

        [$related1, $related2, $related3] = factory(Article::class, 3)->create([
            'user_id' => $john->id,
        ]);

        factory(Article::class, 5)->create([
            'user_id' => $jane->id,
        ]);

        $response = $this->actingAs($john)->get('/api/articles/' . $article->id . '/related');

        $response
            ->assertSuccessful()
            ->assertJsonFragment(['title' => $related1->title])
            ->assertJsonFragment(['title' => $related2->title])
            ->assertJsonFragment(['title' => $related3->title]);
    }

    public function test_attempt_to_get_related_article_of_not_exist_article()
    {
        $john = factory(User::class)->create();

        $response = $this->actingAs($john)->get('/api/articles/0/related');

        $response
            ->assertStatus(404)
            ->assertSee('Can not find article entity.');
    }
}
