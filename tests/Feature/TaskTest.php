<?php

namespace Tests\Feature;

use App\Task;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_see_tasks_list()
    {
        $user = factory(User::class)->create();
        $tasks = factory(Task::class, 3)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->actingAs($user)->get('/tasks');

        $response
            ->assertSuccessful()
            ->assertViewIs('tasks.list');
    }

    public function test_user_cannot_see_task_not_belonging_to_him()
    {
        [$john, $jane] = factory(User::class, 2)->create();

        factory(Task::class)->create([
            'user_id' => $john->id,
        ]);

        factory(Task::class)->create([
            'user_id' => $jane->id,
        ]);

        $response = $this->actingAs($john)->get('/api/tasks?page=1&perPage=15&q=&sortBy=id&descending=true');

        $response->assertSuccessful()
            ->assertJsonFragment(['current_page' => 1])
            ->assertJsonFragment(['per_page' => 15])
            ->assertJsonFragment(['total' => 1]);
    }

    public function test_user_can_add_new_task()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->post('/api/tasks', ['name' => 'test']);
        $response->assertJsonFragment(['name' => $user->tasks[0]->name]);
        $response->assertJsonFragment(['id' => $user->tasks[0]->id]);
        $this->assertEquals($user->tasks->count(), 1);
    }

    public function test_user_can_update_task_name()
    {
        $user = factory(User::class)->create();

        $taskToUpdate = [
            'name' => 'updatedTask',
        ];

        factory(Task::class, 5)->create([
            'user_id' => $user->id,
        ]);

        $task = $user->tasks->random();

        $response = $this->actingAs($user)->put('/api/tasks/' . $task->id, $taskToUpdate);
        $response->assertSuccessful();
        $response->assertSee($task->id);
        $this->assertSame($task->fresh()->name, $taskToUpdate['name']);
    }

    public function test_user_can_update_task_status()
    {
        $user = factory(User::class)->create();

        factory(Task::class, 5)->create([
            'user_id' => $user->id,
            'completed' => true,
        ]);

        $task = $user->tasks->random();

        $response = $this->actingAs($user)->patch('/api/tasks/' . $task->id . '/toggle-status', ['completed' => false]);
        $response->assertExactJson(['status' => true, 'completed' => '0']);
        $this->assertFalse(
            !!$task->refresh()->completed
        );
    }

    public function test_user_cannot_update_task_not_belonging_to_him()
    {
        $john = factory(User::class)->create();
        $jane = factory(User::class)->create();

        $taskToUpdate = [
            'name' => 'updatedTask',
        ];

        factory(Task::class, 3)->create([
            'user_id' => $john->id,
        ]);

        factory(Task::class, 3)->create([
            'user_id' => $jane->id,
        ]);

        $response = $this->actingAs($jane)->put('/api/tasks/' . $john->tasks->random()->id, $taskToUpdate);
        $response->assertForbidden();
    }

    public function test_user_cannot_update_task_which_does_not_exist()
    {
        $user = factory(User::class)->create();

        $taskToUpdate = [
            'name' => 'updatedTask',
        ];

        factory(Task::class, 5)->create([
            'user_id' => $user->id,
        ]);

        $response = $this->actingAs($user)->patch('/api/tasks' . 9999999, $taskToUpdate);
        $response->assertNotFound();
    }

    public function test_user_can_delete_task()
    {
        $user = factory(User::class)->create();

        factory(Task::class, 5)->create([
            'user_id' => $user->id,
        ]);

        $task = $user->tasks->random();

        $response = $this->actingAs($user)->delete('/api/tasks/' . $task->id);
        $response->status(204);
        $this->assertEquals($user->refresh()->tasks->count(), 4);
    }

    public function test_user_cannot_delete_task_not_belonging_to_him()
    {
        [$john, $jane] = factory(User::class, 2)->create();

        factory(Task::class)->create([
            'user_id' => $john->id,
        ]);

        factory(Task::class)->create([
            'user_id' => $jane->id,
        ]);

        $response = $this->actingAs($jane)->delete('/api/tasks/' . $john->tasks()->first()->id);
        $response->assertSuccessful();
        $this->assertSame($john->tasks()->count(), 1);
        $this->assertSame($jane->tasks()->count(), 1);
    }

    public function test_user_cannot_delete_task_which_does_not_exist()
    {
        $user = factory(User::class)->create();

        factory(Task::class)->create([
            'user_id' => $user->id,
        ]);

        $beforAttemptToDelete = Task::count();

        $response = $this->actingAs($user)->delete('/api/tasks/' . 9999999);
        $response->assertSuccessful();
        $this->assertSame($beforAttemptToDelete, Task::count());
    }
}
