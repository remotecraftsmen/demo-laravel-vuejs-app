@extends('layouts.app')

@section('content')
        <div class="row justify-content-center">
            <div class="col-md-12">
                <articles-list></articles-list>
            </div>
        </div>
@endsection
