@extends('layouts.app')

@section('content')
        <div class="row justify-content-center">
            <div class="col-md-12">
                <single-article :article-id="{{ $articleId }}"></single-article>
            </div>
        </div>
@endsection
