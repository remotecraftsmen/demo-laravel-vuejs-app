<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@3.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/main.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <v-app>
            <v-app-bar app>
                @guest
                    <vue-menu app-name="{{ config('app.name', 'Laravel') }}" user="guest"></vue-menu>
                @else
                    <vue-menu app-name="{{ config('app.name', 'Laravel') }}"></vue-menu>
                @endguest
            </v-app-bar>
            <v-content class="pt-12">
                <v-container fluid>
                    <v-row justify="center">
                        <v-col cols="12" sm="11" md="9" lg="7">
                            @yield('content')
                            <flash-message class="messages-class"></flash-message>
                        </v-col>
                    </v-row>
                </v-container>
            </v-content>
            <v-footer app>
            </v-footer>
        </v-app>
    </div>
</body>
</html>
