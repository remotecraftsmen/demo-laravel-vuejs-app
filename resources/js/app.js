import Vue from 'vue';
import VueFlashMessage from 'vue-flash-message';
import vuetify from './plugins/vuetify';
require('./bootstrap');
require('vue-flash-message/dist/vue-flash-message.min.css');

window.Vue = Vue;
Vue.use(VueFlashMessage);
Vue.prototype.$eventHub = new Vue(); // Global event bus

Vue.component('vue-menu', require('./components/Menu.vue'));
Vue.component('tasks-list', require('./views/TasksList.vue'));
Vue.component('articles-list', require('./views/ArticlesList.vue'));
Vue.component('single-article', require('./views/SingleArticle.vue'));
Vue.component('login-form', require('./views/Login.vue'));
Vue.component('register-form', require('./views/Register.vue'));

const app = new Vue({
    el: '#app',
    vuetify
});
