<?php

use App\Article;
use App\User;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'content' => $faker->text($maxNbChars = 1000),
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
    ];
});
