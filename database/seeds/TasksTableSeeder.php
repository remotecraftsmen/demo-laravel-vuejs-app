<?php

use App\Task;
use App\User;
use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::limit(20)->get();

        foreach ($users as $user) {
            factory(Task::class, 5)->create([
                'user_id' => $user->id,
            ]);
        }
    }
}
