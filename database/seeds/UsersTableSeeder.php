<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrCreate(['email' => 'test@example.com'],
            [
                'name' => 'Test Account',
                'email' => 'test@example.com',
                'password' => bcrypt('password'),
            ]
        );

        factory(User::class, 19)->create();
    }
}
