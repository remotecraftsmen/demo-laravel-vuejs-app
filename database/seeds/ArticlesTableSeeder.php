<?php

use App\Article;
use App\User;
use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::limit(20)->get();

        foreach ($users as $user) {
            factory(Article::class, 10)->create([
                'user_id' => $user->id,
            ]);
        }
    }
}
