<?php

namespace App\Exceptions;

use Exception;

class ModelNotFoundDuringBinding extends Exception
{
    protected $entityName;

    public function __construct(string $entityName)
    {
        $this->entityName = $entityName;
    }

    public function render($request)
    {
        return response($this->entityName . ' entity not found.', 404);
    }
}
