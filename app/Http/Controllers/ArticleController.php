<?php

namespace App\Http\Controllers;

use App\Article;

class ArticleController extends Controller
{
    public function index()
    {
        return view('articles.list');
    }

    public function show(Article $article)
    {
        return view('articles.show', ['articleId' => $article->id]);
    }
}
