<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\Unauthorized;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTask;
use App\Http\Requests\UpdateTask;
use App\Repositories\TaskRepositoryInterface;
use App\Task;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    protected $taskRepository;

    public function __construct(TaskRepositoryInterface $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function index(Request $request)
    {
        try {
            $sortOrder = $request->descending === 'true' ? 'desc' : 'asc';

            $tasks = $this->taskRepository->index($request->perPage, $request->q, $request->sortBy, $sortOrder);

            return compact('tasks');
        } catch (Exception $e) {
            return response('Operation failed', 400);
        }
    }

    public function store(StoreTask $request)
    {
        try {
            $attrs = $request->validated();
            $task = $this->taskRepository->store($attrs);

            return compact('task');
        } catch (Exception $e) {
            return response('Operation failed', 400);
        }
    }

    public function update(Task $task, UpdateTask $request)
    {
        try {
            $attrs = $request->validated();
            $this->taskRepository->update($task, $attrs);

            return $task->id;
        } catch (Unauthorized $e) {
            return response('Access denied', 403);
        } catch (Exception $e) {
            return response('Operation failed', 400);
        }
    }

    public function destroy(int $id)
    {
        try {
            $this->taskRepository->destroy($id);

            response('', 204);
        } catch (Exception $e) {
            return response('Operation failed', 400);
        }
    }

    public function toggleStatus(Task $task)
    {
        try {
            $task = $this->taskRepository->toggleStatus($task);

            return response()->json(['status' => true, 'completed' => $task->completed]);
        } catch (Unauthorized $e) {
            return response('Access denied', 403);
        } catch (Exception $e) {
            return response('Operation failed', 400);
        }
    }
}
