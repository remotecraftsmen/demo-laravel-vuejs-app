<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreArticle;
use App\Repositories\ArticleRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    protected $articleRepository;

    public function __construct(ArticleRepositoryInterface $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    public function index(Request $request)
    {
        try {
            $onlyMy = $request->onlyMy === 'true';
            $articles = $this->articleRepository->index($request->q, $onlyMy);

            return compact('articles');
        } catch (Exception $e) {
            return response('Operation failed.', 400);
        }
    }

    public function store(StoreArticle $request)
    {
        $attrs = $request->only(['title', 'content']);
        $image = $request->file('image');

        try {
            return $this->articleRepository->store($attrs, $image);
        } catch (Exception $e) {
            return response('Operation failed.', 400);
        }
    }

    public function show(int $id)
    {
        try {
            return $this->articleRepository->show($id);
        } catch (ModelNotFoundException $e) {
            return response('Can not find article entity.', 404);
        } catch (Exception $e) {
            return response('Operation failed.', 400);
        }
    }

    public function getRelated(int $id)
    {
        try {
            $related = $this->articleRepository->getRelated($id);

            return compact('related');
        } catch (ModelNotFoundException $e) {
            return response('Can not find article entity.', 404);
        } catch (Exception $e) {
            return response('Operation failed.', 400);
        }
    }
}
