<?php

namespace App\Repositories;

use App\Task;

interface TaskRepositoryInterface
{
    public function index(string $perPage, ?string $searched, string $sortBy, string $sortOrder);

    public function store(array $taskData);

    public function update(Task $task, array $taskData);

    public function destroy(int $id);

    public function toggleStatus(Task $task);
}
