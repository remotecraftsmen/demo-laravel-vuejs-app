<?php

namespace App\Repositories;

use App\Article;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ArticleRepository implements ArticleRepositoryInterface
{
    public function index(?string $searched, bool $onlyMy)
    {
        if ($onlyMy) {
            $articles = auth()->user()->articles();
        } else {
            $articles = Article::query();
        }

        $articles = $articles->where(function ($q) use ($searched) {
            if (!empty($searched)) {
                $q->where('title', 'like', '%' . $searched . '%');
                $q->orWhere('content', 'like', '%' . $searched . '%');
            }
        })
            ->with('user')
            ->orderBy('id', 'desc')
            ->paginate(9);

        return $articles;
    }

    public function store(array $articleData, ?UploadedFile $image)
    {
        $user = auth()->user();

        $article = $user->addArticle($articleData);

        if (!empty($image)) {
            $name = $article->id . str_random(10);
            $extension = $image->extension();

            $path = Storage::disk('public')->putFileAs(
                'articles',
                $image,
                $name . '.' . $extension
            );

            $article->update(['image' => $path]);
        }

        return $article->fresh();
    }

    public function show(int $id)
    {
        return Article::with('user')->findOrFail($id);
    }

    public function getRelated(int $id)
    {
        $article = Article::findOrFail($id);

        $related = Article::with('user')
            ->where('user_id', $article->user_id)
            ->where('id', '<>', $id)
            ->inRandomOrder()
            ->limit(3)
            ->get();

        if (count($related) < 3) {
            $related = $related->merge(
                Article::with('user')
                    ->inRandomOrder()
                    ->limit(3 - count($related))
                    ->get()
            );
        }

        return $related;
    }
}
