<?php

namespace App\Repositories;

use Illuminate\Http\UploadedFile;

interface ArticleRepositoryInterface
{
    public function index(?string $searched, bool $onlyMy);

    public function store(array $articleData, ?UploadedFile $image);

    public function show(int $id);

    public function getRelated(int $id);
}
