<?php

namespace App\Repositories;

use App\Exceptions\Unauthorized;
use App\Task;
use Illuminate\Support\Facades\Gate;

class TaskRepository implements TaskRepositoryInterface
{
    public function index(string $perPage, ?string $searched, string $sortBy, string $sortOrder)
    {
        $user = auth()->user();
        $perPage = $perPage === 'All' ? $user->tasks()->count() : (int) $perPage;

        $tasks = $user->tasks()
            ->where(function ($q) use ($searched) {
                if (!empty($searched)) {
                    $q->where('name', 'like', '%' . $searched . '%');
                }
            })
            ->orderBy($sortBy, $sortOrder)->paginate($perPage);

        return $tasks;
    }

    public function store(array $taskData)
    {
        $user = auth()->user();

        $taskData['completed'] = false;

        return $user->addTask($taskData);
    }

    public function update(Task $task, array $taskData)
    {
        $user = auth()->user();

        if (Gate::forUser($user)->allows('update', $task)) {
            return $task->update($taskData);
        }

        throw new Unauthorized();
    }

    public function destroy(int $id)
    {
        $task = Task::find($id);

        if (!$task) {
            return;
        }

        $user = auth()->user();

        if (Gate::forUser($user)->allows('update', $task)) {
            $task->delete();
        }
    }

    public function toggleStatus(Task $task)
    {
        $user = auth()->user();

        if (Gate::forUser($user)->allows('update', $task)) {
            $task->update(['completed' => !$task->completed]);
            return $task->fresh();
        }

        throw new Unauthorized();
    }
}
