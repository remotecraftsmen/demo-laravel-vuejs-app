# Todo App

Basic todo app written in Laravel, Vue.js and Vuetify.

## Prerequisites

-   NPM - v6.4.1
-   Docker - v18.06.1 (optional)

## DEMO

Live demo available at https://laravel57-vuejs-demo.rmtcfm.com

#### Demo account:

E-mail address: `test@example.com`  
Password: `password`

## Installation

### Using docker:

#### 1. Clone the repository

`git clone git@bitbucket.org:remotecraftsmen/basic-laravel-app.git`

#### 2. Get config files

`./exec.sh configure`

#### 3. Set your .env file

#### 4. Get your app key

`./exec.sh init`

#### 5. Run docker

`./exec.sh up`

#### 6. Install vendor

`./exec.sh composer install`

#### 7. Install packages

`npm run watch` or
`npm run prod` (production)

#### 8. Run the database migrations

`./exec.sh artisan migrate`

#### 9. Run the database seeders

`./exec.sh artisan db:seed`

#### 10. Link storage

`./exec.sh artisan storage:link`

##

### Without docker:

#### 1. Clone the repository

`git clone git@bitbucket.org:remotecraftsmen/basic-laravel-app.git`

#### 2. Get config files

`./exec.sh configure`

#### 3. Set your .env file

#### 4. Generate your app key

`php artisan key:generate`

#### 5. Install vendor

`composer install`

#### 6. Install packages

`npm run watch` or
`npm run prod` (production)

#### 7. Run the database migrations

`php artisan migrate`

#### 8. Run the database seeders

`php artisan db:seed`

#### 9. Link storage

`php artisan storage:link`
