<?php

Route::middleware(['auth.api'])->namespace('Api')->group(function () {
    Route::get('/tasks', 'TaskController@index');
    Route::post('/tasks', 'TaskController@store');
    Route::put('/tasks/{task}', 'TaskController@update');
    Route::delete('/tasks/{id}', 'TaskController@destroy');
    Route::patch('/tasks/{task}/toggle-status', 'TaskController@toggleStatus');

    Route::get('/articles', 'ArticleController@index');
    Route::post('/articles', 'ArticleController@store');
    Route::get('/articles/{id}', 'ArticleController@show');
    Route::get('/articles/{id}/related', 'ArticleController@getRelated');
});
